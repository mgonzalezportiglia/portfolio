## 📫 Contact with me:
<p align="left">
<a href = "https://www.linkedin.com/in/matias-gonzalez-portiglia-24081994"><img src="https://img.icons8.com/fluent/48/000000/linkedin.png"/></a>
</p>

## 🙋🏻‍♂️ About Me

- 🖇️ Hi everyone, my name is Matias and i’m Full stack / Mobile developer since 7 years ago. I'm 28 years old.
- 🇦🇷 I'm Argentinian.
- ⚽️ I like to play futbol and do some exercise.
- 🧉 Also i drink a lot of mate.

## 👨🏻‍💻 Languages & Frameworks:
### _Mobile_

- <img src="/assets/swift.png" alt="Alt text" title="Swift" width="80px"></img>  <img src="/assets/swiftui.png" alt="Alt text" title="SwiftUI" width="120px"></img>  <img src="/assets/react_native.png" alt="Alt text" title="React Native" width="80px"></img>

### _Backend_

- <img src="/assets/scala.jpeg" alt="Alt text" title="Scala" width="80px"></img>  <img src="/assets/spring_boot.png" alt="Alt text" title="Spring Boot" width="200px"></img>

### _Web front-end_

- <img src="/assets/javascript.png" alt="Alt text" title="Javascript" width="80px"></img>  <img src="/assets/react.png" alt="Alt text" title="React" width="200px"></img>

## 🚀 Recent Portfolio Work
### _iOS, iPad and macOS_

| Device | Project repository | Resume | |
| ------ | ------ | ------ | ------ |
| 📱 | [Lets chating App - Github Project](https://github.com/mgonzalezportiglia/Lets-Chating-App#lets-chating-app) | An App for chating with users from the app. The authentication of this user was maded with Firebase Authentication (Email and Password) and the conversations between each of them are stored in Firebase Firestore. | [Pictures example](https://github.com/mgonzalezportiglia/Lets-Chating-App#-pictures-example) |
| 📱 | [Spending tracker App - Github Project](https://github.com/mgonzalezportiglia/SpendingTracker/tree/master#spending-tracker) | Swift application that uses Core Data to persist the expenses we made. This expenses are related with the credit cards that we have persisted to. | [Pictures example](https://github.com/mgonzalezportiglia/SpendingTracker/tree/master#-pictures-example) |

> _In this moment i’m working as a fullstack developer in AFIP with lenguages like React,
> Typescript, HTML, CSS, Scala.
> I like to be constantly updated with new technologies in order to apply them for my
> current work and learn the more i can the things i like, and for do that i am a self taught
> learner who take advantage of online courses like Udemy (React, Angular, IONIC,
> ReactNative, GraphQL, Swift) and also good books from people around the world (ATC
> Android, Hacking With Swift) to go deeper into the mobile world._

